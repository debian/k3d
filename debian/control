Source: k3d
Section: graphics
Priority: optional
Standards-Version: 4.2.1
Maintainer: Manuel A. Fernandez Montecelo <mafm@debian.org>
Homepage: http://www.k-3d.org/
Vcs-Git: https://salsa.debian.org/debian/k3d.git
Vcs-Browser: https://salsa.debian.org/debian/k3d
Rules-Requires-Root: no
Build-Depends: debhelper (>= 11~),
               asciidoc,
               cmake (>> 2.6),
               dh-python,
               docbook-xml,
               docbook-xsl,
               doxygen,
               gettext,
               graphviz,
               lib3ds-dev,
               libboost-date-time-dev (>= 1.61),
               libboost-dev (>= 1.61),
               libboost-program-options-dev (>= 1.61),
               libboost-python-dev (>= 1.61),
               libboost-regex-dev (>= 1.61),
               libboost-system-dev (>= 1.61),
               libboost-test-dev (>= 1.61),
               libcairomm-1.0-dev,
               libcgal-dev (>= 4.0) [!armel],
               libdbus-glib-1-dev,
               libfreetype6-dev,
               libftgl-dev,
               libgl1-mesa-dev | libgl-dev,
               libglew-dev (>= 2.0.0~),
               libglibmm-2.4-dev,
               libglu1-mesa-dev | libglu-dev,
               libgraphviz-dev,
               libgtkglext1-dev,
               libgtkmm-2.4-dev,
               libgts-dev,
               libjpeg-dev,
               libmagick++-dev,
               libode-dev,
               libopenexr-dev,
               libpng-dev,
               librsvg2-bin,
               libsigc++-2.0-dev,
               libtiff-dev,
               libxml2-dev | libexpat1-dev,
               pkg-config,
               python-dev,
               uuid-dev,
               xsltproc,
               zlib1g-dev


Package: k3d
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         k3d-data (= ${source:Version})
Replaces: k3d-dev (<< 0.8.0.1)
Breaks: k3d-dev (<< 0.8.0.1)
Description: 3D modeling and animation system, binary files
 K-3D is free-as-in-freedom 3D modeling and animation software. It combines
 flexible plugins with a visualization pipeline architecture, making K-3D a
 versatile and powerful tool for artists.
 .
 This package contains binaries and libraries.

Package: k3d-data
Architecture: all
Replaces: k3d (<< 0.8.0.2-2)
Breaks: k3d (<< 0.8.0.2-2)
Depends: ${misc:Depends},
         ttf-bitstream-vera (>= 1.10)
Description: 3D modeling and animation system, data files
 K-3D is free-as-in-freedom 3D modeling and animation software. It combines
 flexible plugins with a visualization pipeline architecture, making K-3D a
 versatile and powerful tool for artists.
 .
 This package contains data needed by the binaries (shaders, GUI elements, ...).
