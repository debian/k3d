#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# needed for CGAL, otherwise fails compiling with:
# Explanation: Wrong rounding: did you forget the  -frounding-math  option if you use GCC (or  -fp-model strict  for Intel)?
CGAL_COMPILER_FLAG := -frounding-math

export DEB_BUILD_MAINT_OPTIONS   := hardening=+pie,+bindnow
export DEB_CFLAGS_MAINT_APPEND   := -pipe -Wall $(shell dpkg-buildflags --get CPPFLAGS) $(CGAL_COMPILER_FLAG)
export DEB_CXXFLAGS_MAINT_APPEND := -pipe -Wall $(shell dpkg-buildflags --get CPPFLAGS) $(CGAL_COMPILER_FLAG)
export DEB_LDFLAGS_MAINT_APPEND  := -Wl,-z,defs -Wl,--as-needed


# inotify in multiarch system -- K3DFindInotify.cmake script tries to
# find 'sys/inotify.h' under '/usr/include' exclusively, by default, so
# it fails to find and thus disables building of the k3d-inotify.module
ifeq (linux,$(DEB_BUILD_ARCH_OS))
	CMAKE_INOTIFY += -DK3D_INOTIFY_INCLUDE_DIR="/usr/include/$(DEB_BUILD_MULTIARCH)"
endif


# Setting $HOME, needed to build the guide (otherwise it fails in Debian
# buildds, where $HOME cannot be used)
BUILD_HOME = $(CURDIR)/debian/build_home


%:
	dh $@ --buildsystem=cmake --with python2

override_dh_auto_configure:
	dh_auto_configure -- $(CMAKE_INOTIFY)

override_dh_auto_build:
	mkdir -p $(BUILD_HOME)
	HOME=$(BUILD_HOME) dh_auto_build

override_dh_auto_install:
	dh_auto_install

        # Strip plugins and libraries, dh_link doesn't work with all of them
	find debian/tmp/usr/lib -name \*.module -exec strip --strip-unneeded --remove-section=.comment --remove-section=.note {} \;

        # Remove files, we're not going to ship them because it would be only
        # useful for plugin developers and upstream discourages it
	rm -fr debian/tmp/usr/include debian/tmp/usr/lib/k3d/include
	rm -f debian/tmp/usr/lib/*.so

        # Remove qtui data files, qtui is not built
	rm -fr debian/tmp/usr/share/k3d/qtui

        # Move guide
	mkdir -p debian/tmp/usr/share/doc/k3d
	mv debian/tmp/usr/share/k3d/guide debian/tmp/usr/share/doc/k3d

        # Quell lintian warning "duplicate-font-file" by removing the files and
        # symlinking to the ones in the package tff-bitstream-vera.  Use
        # dh_link, since paths must be relative when elements in the path are
        # common (e.g. /usr/)
	rm -f debian/tmp/usr/share/k3d/fonts/Vera*.ttf

override_dh_install:
	dh_install --list-missing

override_dh_shlibdeps:
	dh_shlibdeps -- debian/k3d/usr/lib/k3d/plugins/*.module

override_dh_clean:
	dh_clean
	rm -rf $(BUILD_HOME)
